package com.mindtree.converter;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.mindtree.constants.Constants;
import com.mindtree.pojo.hybris.products.ProductHybris;
import com.mindtree.pojo.hybris.shippingAddress.Addresses;
import com.mindtree.pojo.hybris.shippingAddress.DeliveryModes;
import com.mindtree.pojo.mindtree.cart.Cart;
import com.mindtree.pojo.mindtree.product.Facets;
import com.mindtree.pojo.mindtree.product.ProductDetail;
import com.mindtree.pojo.mindtree.product.ProductMindtree;
import com.mindtree.pojo.mindtree.product.Products;
import com.mindtree.pojo.mindtree.product.Values;
import com.mindtree.pojo.mindtree.shippingAddress.ShippingAddress;
import com.mindtree.pojo.mindtree.shippingAddress.ShippingAddressList;
import com.mindtree.pojo.mindtree.shippingAddress.ShippingMethodList;
import com.mindtree.pojo.mindtree.shippingAddress.ShippingMethods;

public class Converter {
	
	public static ProductMindtree convertHybrisSearchResultsToMindtreeFmt(ProductHybris prodHybris){
		
		Pattern pattern = Pattern.compile("(https?://)([^:^/]*)(:\\d*)?(.*)?");
		Matcher matcher = pattern.matcher(Constants.HYBRIS_SEARCH_URL);
		matcher.find();
		String protocol = matcher.group(1);            
		String domain   = matcher.group(2);
		String port = matcher.group(3);
		String baseUrl=protocol+domain+port;
		ProductMindtree prodmind = new ProductMindtree();
		//for products list
		Products[] productsM = new Products[prodHybris.getProducts().length];
		com.mindtree.pojo.hybris.products.Products[] productsH = prodHybris.getProducts();
		for(int i=0; i<prodHybris.getProducts().length; i++){
			Products prod = new Products();
			prod.setCode(productsH[i].getCode());
			prod.setName(productsH[i].getName());
			prod.setDescription((productsH[i].getDescription()));
			prod.setImage(baseUrl+productsH[i].getImages()[0].getUrl());
			prod.setPrice(productsH[i].getPrice().getValue());
			prod.setCurrency(productsH[i].getPrice().getCurrencyIso());
			productsM[i] = prod;
		}
		//for facets list
		Facets[] facetM = new Facets[prodHybris.getFacets().length];
		com.mindtree.pojo.hybris.products.Facets[] facetH = prodHybris.getFacets();
		for(int i=0; i < prodHybris.getFacets().length; i++){
			Facets facet = new Facets();
			facet.setName(facetH[i].getName());
			Values[] valuesM = new Values[facetH[i].getValues().length];
			com.mindtree.pojo.hybris.products.Values[]  valuesH = facetH[i].getValues();
			for(int j=0; j < valuesM.length; j++){
				Values val = new Values();
				val.setName(valuesH[j].getName());
				valuesM[j]=val;
			}
			facet.setValues(valuesM);
			facetM[i] = facet;
		}
		prodmind.setFacets(facetM);
		prodmind.setProducts(productsM);
		return prodmind;
	}
	
	/*public ProductMindtree convertMagentoProductToMindtreeFmt(ProductMagento prod){
		String baseUrl="http://magentopoc1-vm1.cloudapp.net/pub/media/catalog/product/cache/1/image/e9c3970ab036de70892d86c6d221abfe/";
		ProductMindtree prodMind = new ProductMindtree();
		Products[] productM = new Products[prod.getItems().length];
		int i=-1;
		for (Items productMag : prod.getItems()) {
			Products prodM = new Products();
			prodM.setCode(productMag.getSku());
			prodM.setName(productMag.getName());
			prodM.setPrice(productMag.getPrice());
			//hard coding currency as we are not getting it from Magento Response
			prodM.setCurrency("USD");
			Custom_attributes[] custAttr = new Custom_attributes[productMag.getCustom_attributes().length];
			for (Custom_attributes custom_attributes : productMag.getCustom_attributes()) {
				if(custom_attributes.getAttribute_code().equals("description")){
					prodM.setDescription(custom_attributes.getValue());
					continue;
				}
				if(custom_attributes.getAttribute_code().equals("image")){
					prodM.setImage(baseUrl+custom_attributes.getValue());
				}
			}
			i++;
			productM[i] = prodM;
		}
		prodMind.setProducts(productM);
		return prodMind;
	}*/
	
	public static Cart convertHybrisCartToMindtree(com.mindtree.pojo.hybris.cart.Cart cartHyb){
		Cart cartMind = new Cart();
		cartMind.setCartid(cartHyb.getGuid());
		return cartMind;
	}
	
	public static ProductDetail convertHybrisProdDetailToMindtree(com.mindtree.pojo.hybris.productDetails.ProductDetail prodDetailH){
		ProductDetail prodDetailM = new ProductDetail();
		prodDetailM.setName(prodDetailH.getName());
		prodDetailM.setDescription(prodDetailH.getDescription());
		prodDetailM.setSummary(prodDetailH.getSummary());
		prodDetailM.setPrice(prodDetailH.getPrice().getFormattedValue());
		prodDetailM.setCurrency(prodDetailH.getPrice().getCurrencyIso());
		prodDetailM.setStock(prodDetailH.getStock().getStockLevelStatus());
		return prodDetailM;
	}
	
	public static ProductDetail getProductDetailFromProduct(ProductMindtree prodM){
		ProductDetail prodDetailM = new ProductDetail();
		if(prodM != null){
		prodDetailM.setName(prodM.getProducts()[0].getName());
		prodDetailM.setDescription(prodM.getProducts()[0].getDescription());
		prodDetailM.setSummary(prodM.getProducts()[0].getDescription());
		prodDetailM.setPrice(prodM.getProducts()[0].getPrice());
		prodDetailM.setCurrency(prodM.getProducts()[0].getCurrency());
		//Magento does'nt return stock. Chat Bot does'nt expect this as of now. This is for Future purpose
		prodDetailM.setStock("InStock");
		return prodDetailM;
		}else return prodDetailM;
	}
	
	public static ShippingAddressList convertHybrisShipAddrToMindShipAddr(com.mindtree.pojo.hybris.shippingAddress.ShippingAddressList addrListHyb){
		ShippingAddressList addrMindList = new ShippingAddressList();
		Addresses[] addrHyb = addrListHyb.getAddresses();
		ShippingAddress[] addrMind = new ShippingAddress[addrHyb.length];
		for(int i =0; i < addrHyb.length; i++){
			ShippingAddress addr = new ShippingAddress();
			addr.setAddressId(addrHyb[i].getId());
			addr.setCountry(addrHyb[i].getCountry().getIsocode());
			addr.setFirstName(addrHyb[i].getFirstName());
			addr.setTown(addrHyb[i].getTown());
			addr.setLastName(addrHyb[i].getLastName());
			addr.setLine1(addrHyb[i].getLine1());
			addr.setLine2(addrHyb[i].getLine2());
			addr.setPostalCode(addrHyb[i].getPostalCode());
			addrMind[i] = addr;
		}
		addrMindList.setShipAddr(addrMind);
		return addrMindList;
	}

	public static ShippingAddressList convertMagentoShipAddrToMindShipAddr(
			com.mindtree.pojo.magento.shippingAddress.ShippingAddressList addrListMagento) {
		ShippingAddressList addrMindList = new ShippingAddressList();
		com.mindtree.pojo.magento.shippingAddress.ShippingAddress[] addrMagento = addrListMagento.getShippingAddress();
		ShippingAddress[] addrMind = new ShippingAddress[addrMagento.length];
		for(int i = 0; i < addrMagento.length; i ++){
			ShippingAddress addr = new ShippingAddress();
			addr.setCountry(addrMagento[i].getCountry_id());
			addr.setFirstName(addrMagento[i].getFirstname());
			addr.setTown(addrMagento[i].getCity());
			addr.setLastName(addrMagento[i].getLastname());
			addr.setLine1(addrMagento[i].getStreet()[0].toString());
			addr.setPostalCode(addrMagento[i].getPostcode());
			addr.setAddressId(addrMagento[i].getRegion_id());
			addrMind[i] = addr;
		}
		addrMindList.setShipAddr(addrMind);
		return addrMindList;
	}

	public static ShippingMethodList convertHybrisShipMethodToMind(
			com.mindtree.pojo.hybris.shippingAddress.ShippingMethods shipMethHybris) {
		ShippingMethods[] shipMethMind = new ShippingMethods[shipMethHybris.getDeliveryModes().length];
		DeliveryModes[] deliveryModes = shipMethHybris.getDeliveryModes();
		ShippingMethodList list = new ShippingMethodList();
		for(int i = 0; i < shipMethHybris.getDeliveryModes().length; i++){
			ShippingMethods shipMeth = new ShippingMethods();
			shipMeth.setCode(deliveryModes[i].getCode());
			shipMeth.setName(deliveryModes[i].getName());
			if(deliveryModes[i].getDeliveryCost()!=null){
			shipMeth.setPrice(deliveryModes[i].getDeliveryCost().getFormattedValue());
			}
			shipMethMind[i] = shipMeth;
		}
		list.setShipMethods(shipMethMind);
		return list;
	}

}
