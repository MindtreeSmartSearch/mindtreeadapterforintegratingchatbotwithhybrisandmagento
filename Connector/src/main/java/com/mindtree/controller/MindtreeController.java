package com.mindtree.controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mindtree.client.HybrisClient;
import com.mindtree.client.MagentoClient;
import com.mindtree.constants.Constants;
import com.mindtree.converter.Converter;
import com.mindtree.pojo.magento.cart.CartItem;
import com.mindtree.pojo.mindtree.cart.Cart;
import com.mindtree.pojo.mindtree.login.Login;
import com.mindtree.pojo.mindtree.product.ProductDetail;
import com.mindtree.pojo.mindtree.product.ProductMindtree;
import com.mindtree.pojo.mindtree.shippingAddress.ShippingAddressList;
import com.mindtree.pojo.mindtree.shippingAddress.ShippingMethodList;
import com.mindtree.pojo.mindtree.shippingAddress.ShippingMethods;


@Controller
public class MindtreeController {
	
	private static final Logger logger = LoggerFactory.getLogger(MindtreeController.class);
	
	@RequestMapping(value = Constants.PRODUCT_SEARCH, method = RequestMethod.POST)
	public @ResponseBody ProductMindtree formMindtreeJson(@RequestBody String request){
		logger.info("Start Forming Json for product serach results");
		ObjectMapper mapper = new ObjectMapper();
		Map<String, String> requestMap = new HashMap<String, String>();
		try {
			requestMap = mapper.readValue(request, new TypeReference<Map<String,String>>(){});
		} catch (JsonParseException e) {
			logger.error(e.getMessage());
		} catch (JsonMappingException e) {
			logger.error(e.getMessage());
		} catch (IOException e) {
			logger.error(e.getMessage());
		}
		logger.info("printing input request........"+requestMap.toString());
		String url="", searchQuery="";
		if(requestMap.get("commerceSystem").equals("hybris")){
			searchQuery = HybrisClient.formSearchQuery(requestMap);
		url = Constants.HYBRIS_SEARCH_URL.concat(searchQuery).concat(Constants.HYBRIS_SEARCH_FACET);
		logger.info("printing hybris search URL..."+url);
		return Converter.convertHybrisSearchResultsToMindtreeFmt(HybrisClient.getProducts(url));
		}
		else if(requestMap.get("commerceSystem").equals("magento")){
			url = Constants.MAGENTO_SEARCH_URL;
			int count = 1;
			for(Map.Entry<String, String> entry : requestMap.entrySet()){
				String key = entry.getKey();
				if(key.equalsIgnoreCase("searchTerm")){
					key = "search_term";
				}
				url = url.concat("&searchCriteria[filterGroups][0][filters]["+count+"][field]="+key+"&searchCriteria[filterGroups][0][filters]["+count+"][value]="+entry.getValue());
				count++;
			}
			return MagentoClient.getMagentoProducts(url);
		}
		else
		return new ProductMindtree();
	}
	
	@RequestMapping(value = Constants.ANONYMOUS_CART, method = RequestMethod.POST)
	public @ResponseBody Cart getAnonymousCart(@RequestBody String request){
		ObjectMapper mapper = new ObjectMapper();
		Map<String, String> requestMap = null;
		try {
			requestMap = mapper.readValue(request, new TypeReference<Map<String,String>>(){});
		} catch (JsonParseException e) {
			logger.error(e.getMessage());
		} catch (JsonMappingException e) {
			logger.error(e.getMessage());
		} catch (IOException e) {
			logger.error(e.getMessage());
		}
		logger.info("printing input request........"+requestMap.toString());
		Cart cartMind = new Cart();
		if(requestMap.get("commerceSystem").equals("hybris")){
			logger.info("Getting anonymous cart in hybris");
			com.mindtree.pojo.hybris.cart.Cart cartHyb = HybrisClient.getAnonymousCart();
			cartMind = Converter.convertHybrisCartToMindtree(cartHyb);
		}else if(requestMap.get("commerceSystem").equals("magento")){
			logger.info("Getting anonymous cart in magento");
			cartMind.setCartid(MagentoClient.getAnonymouusCart().replaceAll("\"", ""));
		}
		return cartMind;
	}
	
	@RequestMapping(value = Constants.ADD_TO_CART, method = RequestMethod.POST)
	public @ResponseBody String addToAnonymousCart(@RequestBody String request){
		String addToCartMsg = "failure";
		ObjectMapper mapper = new ObjectMapper();
		Map<String, String> requestMap = null;
		try {
			requestMap = mapper.readValue(request, new TypeReference<Map<String,String>>(){});
		} catch (JsonParseException e) {
			logger.error(e.getMessage());
		} catch (JsonMappingException e) {
			logger.error(e.getMessage());
		} catch (IOException e) {
			logger.error(e.getMessage());
		}
		logger.info("printing input request........"+requestMap.toString());
		String url = "";
		if(requestMap.get("commerceSystem").equals("hybris")){
			logger.info("Adding item in anonymous cart in Hybris");
			url = Constants.HYBRIS_ANONYMOUS_CART_URL+"/"+requestMap.get("cartId")+"/entries?code="+requestMap.get("itemId");
			addToCartMsg = HybrisClient.addToAnonymousCart(url);
		}else if(requestMap.get("commerceSystem").equals("magento")){
			logger.info("Adding item in anonymous cart in Magento");
			url = Constants.MAGENTO_ANONYMOUS_CART_URL+requestMap.get("cartId")+"/items";
			CartItem item = new CartItem();
			item.setQty("1");
			item.setQuote_id(requestMap.get("cartId"));
			item.setSku(requestMap.get("itemId"));
			addToCartMsg = MagentoClient.addToAnonymousCart(url,item);
		}
		return addToCartMsg;
	}
	
	@RequestMapping(value=Constants.PRODUCT_DETAIL, method = RequestMethod.POST)
	public @ResponseBody ProductDetail getProductDetail(@RequestBody String request){
		ProductDetail pd = new ProductDetail();
		ObjectMapper mapper = new ObjectMapper();
		Map<String, String> requestMap = null;
		try {
			requestMap = mapper.readValue(request, new TypeReference<Map<String,String>>(){});
		} catch (JsonParseException e) {
			logger.error(e.getMessage());
		} catch (JsonMappingException e) {
			logger.error(e.getMessage());
		} catch (IOException e) {
			logger.error(e.getMessage());
		}
		logger.info("printing input request........"+requestMap.toString());
		if(requestMap.get("commerceSystem").equals("hybris")){
			com.mindtree.pojo.hybris.productDetails.ProductDetail prodDetailH = HybrisClient.getProductDetail(requestMap.get("itemId"));
			pd = Converter.convertHybrisProdDetailToMindtree(prodDetailH);
		}else if (requestMap.get("commerceSystem").equals("magento")){
			ProductMindtree prodMind = MagentoClient.getProductDetail(requestMap.get("itemId"));
			pd = Converter.getProductDetailFromProduct(prodMind);	
		}
		
		return pd;
	}
	
	@RequestMapping(value = Constants.LOGIN, method = RequestMethod.POST)
	public @ResponseBody Login login(@RequestBody String request){
		String accessToken = "";
		ObjectMapper mapper = new ObjectMapper();
		Map<String, String> requestMap = null;
		try{
			requestMap = mapper.readValue(request, new TypeReference<Map<String,String>>(){});
		} catch (JsonParseException e) {
			logger.error(e.getMessage());
		} catch (JsonMappingException e) {
			logger.error(e.getMessage());
		} catch (IOException e) {
			logger.error(e.getMessage());
		}
		logger.info("printing input request........"+requestMap.toString());
		if(requestMap.get("commerceSystem").equals("hybris")){
			accessToken = HybrisClient.login(requestMap);
		}else if (requestMap.get("commerceSystem").equals("magento")){
			accessToken = MagentoClient.login(requestMap);	
		}
		Login login = new Login();
		login.setAccessToken(accessToken);
		return login;
	}
	
	@RequestMapping(value = Constants.MERGE_CART, method = RequestMethod.POST)
	public @ResponseBody Cart mergeCart(@RequestBody String request){
		String loggedInUserCartId = "";
		ObjectMapper mapper = new ObjectMapper();
		Map<String, String> requestMap = null;
		try{
			requestMap = mapper.readValue(request, new TypeReference<Map<String,String>>(){});
		} catch (JsonParseException e) {
			logger.error(e.getMessage());
		} catch (JsonMappingException e) {
			logger.error(e.getMessage());
		} catch (IOException e) {
			logger.error(e.getMessage());
		}
		logger.info("printing input request........"+requestMap.toString());
		if(requestMap.get("commerceSystem").equals("hybris")){
			loggedInUserCartId = HybrisClient.mergeCart(requestMap);
		}else if (requestMap.get("commerceSystem").equals("magento")){
			loggedInUserCartId = MagentoClient.mergeCart(requestMap);	
		}
		Cart cart = new Cart();
		cart.setCartid(loggedInUserCartId);
		cart.getCartid();
		return  cart;
	}
	
	@RequestMapping(value = Constants.GET_SHIPPING_ADDRESS, method = RequestMethod.POST)
	public @ResponseBody ShippingAddressList getShippingAddr(@RequestBody String request){
		ShippingAddressList addrList = new ShippingAddressList();
		ObjectMapper mapper = new ObjectMapper();
		Map<String, String> requestMap = null;
		try{
			requestMap = mapper.readValue(request, new TypeReference<Map<String,String>>(){});
		} catch (JsonParseException e) {
			logger.error(e.getMessage());
		} catch (JsonMappingException e) {
			logger.error(e.getMessage());
		} catch (IOException e) {
			logger.error(e.getMessage());
		}
		logger.info("printing input request........"+requestMap.toString());
		if(requestMap.get("commerceSystem").equals("hybris")){
			addrList = HybrisClient.getShippingAddress(requestMap);
		}else if (requestMap.get("commerceSystem").equals("magento")){
			addrList = MagentoClient.getShippingAddress(requestMap);	
		}
		return addrList;
	}
	
	@RequestMapping(value = Constants.GET_SHIPPING_METHODS, method = RequestMethod.POST)
	public @ResponseBody ShippingMethodList getShippingMethods(@RequestBody String request){
		ShippingMethodList shipMethodList = new ShippingMethodList();
		ObjectMapper mapper = new ObjectMapper();
		Map<String, String> requestMap = null;
		try{
			requestMap = mapper.readValue(request, new TypeReference<Map<String,String>>(){});
		} catch (JsonParseException e) {
			logger.error(e.getMessage());
		} catch (JsonMappingException e) {
			logger.error(e.getMessage());
		} catch (IOException e) {
			logger.error(e.getMessage());
		}
		logger.info("printing input request........"+requestMap.toString());
	if(requestMap.get("commerceSystem").equals("hybris")){
		shipMethodList = HybrisClient.getShippingMethod(requestMap);
	}else if(requestMap.get("commerceSystem").equals("magento")){
		shipMethodList = MagentoClient.getShippingMehod(requestMap);
	}
	return shipMethodList;
	}
	
	@RequestMapping(value = Constants.PLACE_ORDER, method = RequestMethod.POST)
	public @ResponseBody String placeOrder(@RequestBody String request){
		String orderId = "empty";
		ObjectMapper mapper = new ObjectMapper();
		Map<String, String> requestMap = null;
		try{
			requestMap = mapper.readValue(request, new TypeReference<Map<String,String>>(){});
		} catch (JsonParseException e) {
			logger.error(e.getMessage());
		} catch (JsonMappingException e) {
			logger.error(e.getMessage());
		} catch (IOException e) {
			logger.error(e.getMessage());
		}
		logger.info("printing input request........"+requestMap.toString());
		if(requestMap.get("commerceSystem").equals("hybris")){
			//to be implemented
		}else if(requestMap.get("commerceSystem").equals("magento")){
			orderId = MagentoClient.placeOrder(requestMap);
		}
		return orderId;
	}
	
	@RequestMapping(value = Constants.HYBRIS_OAUTH,method = RequestMethod.POST)
	public @ResponseBody ResponseEntity getOAuth(@RequestParam(value="client_id") String client_id,@RequestParam(value="client_secret") String client_secret,
			@RequestParam(value="grant_type") String grant_type,@RequestParam(value="username") String username,@RequestParam(value="password") String password) {
		logger.info("Client Id ="+client_id);
		StringBuffer url = new StringBuffer(Constants.HYBRIS_OAUTH_URL);
		url.append("client_id="+client_id+"&client_secret="+client_secret+"&grant_type="+grant_type+"&username="+username+"&password="+password);
		logger.info("Final url is "+url.toString());
		HttpHeaders header = new HttpHeaders();
		Map<?,?> responseMap = new HashMap();
		HybrisClient hybrisClient = new HybrisClient();
		HttpEntity<String> entity = new HttpEntity<String>("parameters", header);
		try {
			logger.info("URL is " +url);
			return(hybrisClient.getRestTemplate().exchange(url.toString(), HttpMethod.POST, entity, Map.class));
		} catch (Exception e) {
			logger.warn("Exception in making OAuth Request to server");
			e.printStackTrace();
		} 
		ResponseEntity<Map> response= new ResponseEntity(HttpStatus.NOT_FOUND);
		return response;
	}

	
}
