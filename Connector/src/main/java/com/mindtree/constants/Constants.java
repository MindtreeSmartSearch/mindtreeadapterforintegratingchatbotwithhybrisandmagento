package com.mindtree.constants;

public class Constants {

	public static final String PRODUCT_SEARCH = "/rest/products";
	public static final String HYBRIS_PRODUCT_URL ="https://hybris6-poc.cloudapp.net:9002/rest/v2/apparel-uk/products/search?query=books:relevance&fields=facets,products(code,description,name,summary,price,images(url,format))&pageSize=6";
	public static final String MAGENTO_PRODUCT_URL ="http://magentopoc1-vm1.cloudapp.net/index.php/rest/V1/products?searchCriteria[filter_groups][0][filters][0][field]=sku&searchCriteria[filter_groups][0][filters][0][value]=UB17PDF_001498";
	public static final String ANONYMOUS_CART = "/rest/cart/anonymous";
	public static final String ADD_TO_CART = "/rest/cart/addItems";
	public static final String HYBRIS_ANONYMOUS_CART_URL = "https://hybris6-poc.cloudapp.net:9002/rest/v2/apparel-uk/users/anonymous/carts";
	public static final String MAGENTO_ANONYMOUS_CART_URL = "http://magentopoc1-vm1.cloudapp.net/index.php/rest/V1/guest-carts/";
    public static final String PRODUCT_DETAIL = "/rest/product/details";
    public static final String PRODUCT_DETAIL_HYBRIS = "https://hybris6-poc.cloudapp.net:9002/rest/v2/apparel-uk/products/";
    public static final String HYBRIS_SEARCH_URL = "https://hybris6-poc.cloudapp.net:9002/rest/v2/apparel-uk/products/search?";
    public static final String HYBRIS_SEARCH_FACET = "&fields=facets,products(code,description,name,summary,price,images(url,format))&pageSize=6";
    public static final String MAGENTO_SEARCH_URL = "http://magentopoc1-vm1.cloudapp.net/index.php/rest/V1/modified_full_search/?searchCriteria[requestName]=quick_search_container&searchCriteria[filterGroups][0][filters][0][field]=price_dynamic_algorithm&searchCriteria[filterGroups][0][filters][0][value]=auto";
    public static final String LOGIN = "/rest/login";
	public static final String HYBRIS_LOGIN_URL = "https://hybris6-poc.cloudapp.net:9002/authorizationserver/oauth/token?client_id=bot&client_secret=bot123&grant_type=password";
	public static final String MAGENTO_LOGIN_URL = "http://magentopoc1-vm1.cloudapp.net/index.php/rest/V1/integration/customer/token";
	public static final String HYBRIS_MERGE_CART = "https://hybris6-poc.cloudapp.net:9002/rest/v2/apparel-uk/users/";
	public static final String MAGENTO_MERGE_CART = "http://magentopoc1-vm1.cloudapp.net/index.php/rest/V1/merge_carts/me";
	public static final String MERGE_CART = "/rest/cart/merge";
	public static final String GET_SHIPPING_ADDRESS = "rest/customer/shippingAddress";
	public static final String MAGENTO_CUSTOMER = "http://magentopoc1-vm1.cloudapp.net/index.php/rest/V1/customers/me";
	public static final String GET_SHIPPING_METHODS = "rest/customer/shippingMethods";
	public static final String MAGENTO_SHIPPING_METHOD_URL = "http://magentopoc1-vm1.cloudapp.net/index.php/rest/V1/carts/mine/estimate-shipping-methods";
	public static final String PLACE_ORDER = "/rest/cart/placeOrder";
	public static final String MAGENTO_PAYMENT_METH_URL = "http://magentopoc1-vm1.cloudapp.net/index.php/rest/V1/carts/mine/order";
	public static final String MAGENTO_SHIP_INFO_URL = "http://magentopoc1-vm1.cloudapp.net/index.php/rest/V1/carts/mine/shipping-information";
	public static final String MAGENTO_ORDER_DETAILS_URL = "http://magentopoc1-vm1.cloudapp.net/index.php/rest/V1/orders/";
	public static final String HYBRIS_OAUTH_URL = "https://hybris6-poc.cloudapp.net:9002/authorizationserver/oauth/token?";
	public static final String HYBRIS_OAUTH = "/rest/hybris/getOAuth";
}
