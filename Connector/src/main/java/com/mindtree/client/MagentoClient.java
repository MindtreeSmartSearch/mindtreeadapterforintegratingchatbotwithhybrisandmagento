package com.mindtree.client;

import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.net.ssl.SSLContext;

import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustStrategy;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.util.JSONPObject;
import com.fasterxml.jackson.databind.util.JSONWrappedObject;
import com.mindtree.constants.Constants;
import com.mindtree.controller.MindtreeController;
import com.mindtree.converter.Converter;
import com.mindtree.pojo.magento.cart.CartItem;
import com.mindtree.pojo.magento.cart.addToCart;
import com.mindtree.pojo.magento.customer.customerDeatail;
import com.mindtree.pojo.magento.shippingAddress.ShippingAddress;
import com.mindtree.pojo.magento.shippingAddress.ShippingMethod;
import com.mindtree.pojo.mindtree.product.ProductMindtree;
import com.mindtree.pojo.mindtree.shippingAddress.ShippingAddressList;
import com.mindtree.pojo.mindtree.shippingAddress.ShippingMethodList;

public class MagentoClient {
	
	private static final Logger logger = LoggerFactory.getLogger(MindtreeController.class);
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//getMagentoProducts("http://magentopoc1-vm1.cloudapp.net/index.php/rest/V1/modified_full_search/?searchCriteria[requestName]=quick_search_container&searchCriteria[filterGroups][0][filters][0][field]=search_term&searchCriteria[filterGroups][0][filters][0][value]=books&searchCriteria[filterGroups][0][filters][1][field]=price_dynamic_algorithm&searchCriteria[filterGroups][0][filters][1][value]=auto");
		//getProductDetail("CMOG2012");
		//Converter conv = new Converter();
		//conv.getProductDetailFromProduct(getProductDetail("CMOG2012"));
		Map map = new HashMap<String, String>();
		map.put("username", "pooja.puranik@mindtree.com");
		/*map.put("password", "customer123");
		login(map);*/
		map.put("accessToken", "a0vcsy941xija8n511q09fuhsb652tb5");
		//map.put("guestCartId", "c64ab05c57afa5703874c6d2ac7205af");
		//mergeCart(map);
		//getShippingAddress(map);
		System.out.println("order is is"+placeOrder(map));
		
	}
	
	/*public static ProductMagento getProducts(String url) {
		
		ProductMagento prod = new ProductMagento();
		try {
			HttpHeaders header = new HttpHeaders();
			header.add("Authorization", "Bearer 63f1fif2qurlx7isg6f3pknxf4tlqrjb");
			HttpEntity<String> entity = new HttpEntity<String>("parameters", header);
		    //ResponseEntity<ProductMagento> response = getRestTemplate().exchange(ProductURIConstants.MAGENTO_URL, HttpMethod.GET, entity, ProductMagento.class);
			ResponseEntity<ProductMagento> response = getRestTemplate().exchange(url, HttpMethod.GET, entity, ProductMagento.class);
			prod = response.getBody();
		    System.out.println("printing response from magento....."+prod.toString());
		} catch (RestClientException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (KeyManagementException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (KeyStoreException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return prod;
	}*/
	
	public static String getAnonymouusCart(){
		String cartId = "";
		try {
			cartId =  getRestTemplate().postForObject(Constants.MAGENTO_ANONYMOUS_CART_URL, null, String.class);
		} catch (RestClientException e) {
			logger.error(e.getMessage());
		} catch (KeyManagementException e) {
			logger.error(e.getMessage());
		} catch (KeyStoreException e) {
			logger.error(e.getMessage());
		} catch (NoSuchAlgorithmException e) {
			logger.error(e.getMessage());
		}
		return cartId;
	}
	
	//Code to disable certificate check and hence avoiding SSL handshake issues.
	public static RestTemplate getRestTemplate() throws KeyStoreException, NoSuchAlgorithmException, KeyManagementException {
		ObjectMapper mapper =new ObjectMapper(); 
		//Takes care of extra elements in json that cannot be mapped to java class
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		TrustStrategy acceptingTrustStrategy = new TrustStrategy() {
	        @Override
	        public boolean isTrusted(X509Certificate[] x509Certificates, String s) throws CertificateException {
	            return true;
	        }
	    };
	    SSLContext sslContext = org.apache.http.ssl.SSLContexts.custom().loadTrustMaterial(null, acceptingTrustStrategy).build();
	    SSLConnectionSocketFactory csf = new SSLConnectionSocketFactory(sslContext, new NoopHostnameVerifier());
	    CloseableHttpClient httpClient = HttpClients.custom().setSSLSocketFactory(csf).build();
	    HttpComponentsClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory();
	    requestFactory.setHttpClient(httpClient);
	    RestTemplate restTemplate = new RestTemplate(requestFactory);
	    MappingJackson2HttpMessageConverter c = new MappingJackson2HttpMessageConverter();
	    c.setObjectMapper(mapper);
	    restTemplate.getMessageConverters().add(c);
	    return restTemplate;
	}

	public static String addToAnonymousCart(String url, CartItem item) {
		String addToCartMsg = "failure";
		try {
			addToCart cartItem = new addToCart();
			cartItem.setCartItem(item);
			ResponseEntity<String> response =  getRestTemplate().postForEntity(url,cartItem,String.class);
			if(response.getStatusCode().name().toString().equals("OK") && response.getStatusCode().value() == 200){
				addToCartMsg = "success";
			}
		} catch (RestClientException e) {
			logger.error(e.getMessage());
		} catch (KeyManagementException e) {
			logger.error(e.getMessage());
		} catch (KeyStoreException e) {
			logger.error(e.getMessage());
		} catch (NoSuchAlgorithmException e) {
			logger.error(e.getMessage());
		}
		return addToCartMsg;
	}

	public static ProductMindtree getMagentoProducts(String url) {
		ProductMindtree prod = new ProductMindtree();
		HttpHeaders header = new HttpHeaders();
		header.add("Authorization", "Bearer 63f1fif2qurlx7isg6f3pknxf4tlqrjb");
		HttpEntity<String> entity = new HttpEntity<String>("parameters", header);
		try {
			ResponseEntity<ProductMindtree> response = getRestTemplate().exchange(url, HttpMethod.GET, entity, ProductMindtree.class);
			prod = response.getBody();
		} catch (RestClientException e) {
			logger.error(e.getMessage());
		} catch (KeyManagementException e) {
			logger.error(e.getMessage());
		} catch (KeyStoreException e) {
			logger.error(e.getMessage());
		} catch (NoSuchAlgorithmException e) {
			logger.error(e.getMessage());
		}
		return prod;
	}

	public static ProductMindtree getProductDetail(String itemId) {
		String url = Constants.MAGENTO_SEARCH_URL.concat("&searchCriteria[filterGroups][0][filters][1][field]=search_term&searchCriteria[filterGroups][0][filters][1][value]="+itemId);
		logger.info("printing product detail url in magento...."+url);
		ProductMindtree prod = new ProductMindtree();
		HttpHeaders header = new HttpHeaders();
		header.add("Authorization", "Bearer 63f1fif2qurlx7isg6f3pknxf4tlqrjb");
		HttpEntity<String> entity = new HttpEntity<String>("parameters", header);
		try {
			ResponseEntity<ProductMindtree> response = getRestTemplate().exchange(url, HttpMethod.GET, entity, ProductMindtree.class);
			prod = response.getBody();
		} catch (RestClientException e) {
			logger.error(e.getMessage());
		} catch (KeyManagementException e) {
			logger.error(e.getMessage());
		} catch (KeyStoreException e) {
			logger.error(e.getMessage());
		} catch (NoSuchAlgorithmException e) {
			logger.error(e.getMessage());
		}
		return prod;
	}

	public static String login(Map<String, String> requestMap) {
		logger.info("Login with Magento user "+requestMap.get("username"));
		String accessToken = "";
		String url = Constants.MAGENTO_LOGIN_URL;
		// create request body
	    String input = "{   \"username\": \""+requestMap.get("username")+"\",   \"password\": \""+requestMap.get("password")+"\" }";
	    try {
	    	ObjectMapper mapper = new ObjectMapper();
		    JsonNode actObj = mapper.readTree(input);
			accessToken = getRestTemplate().postForObject(url, actObj, String.class);
		} catch (RestClientException e) {
			logger.error(e.getMessage());
		} catch (KeyManagementException e) {
			logger.error(e.getMessage());
		} catch (KeyStoreException e) {
			logger.error(e.getMessage());
		} catch (NoSuchAlgorithmException e) {
			logger.error(e.getMessage());
		} catch (JsonProcessingException e) {
			logger.error(e.getMessage());
		} catch (IOException e) {
			logger.error(e.getMessage());
		}
	    logger.info("printing accessToken - "+accessToken.replaceAll("\"", ""));
		return accessToken.replaceAll("\"", "");
	}
	
	public static String mergeCart(Map requestMap){
		logger.info("Merging guest user cart - "+requestMap.get("guestCartId")+" into "+requestMap.get("username")+"'s cart");
		String loggedInUserCartId = "";
		Map requestMagento = new HashMap<String, String>();
		requestMagento.put("guestQuoteId", requestMap.get("guestCartId"));
		String url = Constants.MAGENTO_MERGE_CART;
		HttpHeaders header = new HttpHeaders();
		header.add("Authorization", "Bearer "+requestMap.get("accessToken"));
		HttpEntity<Map> entity = new HttpEntity<Map>(requestMagento, header);
		try {
			ResponseEntity<String> response = getRestTemplate().exchange(url, HttpMethod.PUT, entity, String.class);
			loggedInUserCartId = response.getBody();
		} catch (RestClientException e) {
			logger.error(e.getMessage());
		} catch (KeyManagementException e) {
			logger.error(e.getMessage());
		} catch (KeyStoreException e) {
			logger.error(e.getMessage());
		} catch (NoSuchAlgorithmException e) {
			logger.error(e.getMessage());
		}
		logger.info("merged cart id - "+loggedInUserCartId.replaceAll("\"", ""));
		return loggedInUserCartId.replaceAll("\"", "");
		
	}

	public static ShippingAddressList getShippingAddress(Map<String, String> requestMap) {
		logger.info("Getting Shipping Address for user - "+requestMap.get("username"));
		String url = Constants.MAGENTO_CUSTOMER;
		HttpHeaders header = new HttpHeaders();
		header.add("Authorization", "Bearer "+requestMap.get("accessToken"));
		HttpEntity<String> entity = new HttpEntity<String>("parameters", header);
		customerDeatail cust = new customerDeatail();
		try {
			ResponseEntity<customerDeatail> response = getRestTemplate().exchange(url, HttpMethod.GET, entity, customerDeatail.class);
			cust = response.getBody();
		} catch (RestClientException e) {
			logger.error(e.getMessage());
		} catch (KeyManagementException e) {
			logger.error(e.getMessage());
		} catch (KeyStoreException e) {
			logger.error(e.getMessage());
		} catch (NoSuchAlgorithmException e) {
			logger.error(e.getMessage());
		}
		ShippingAddress[] addrMagento = cust.getAddresses();
		com.mindtree.pojo.magento.shippingAddress.ShippingAddressList addrListMagento = new com.mindtree.pojo.magento.shippingAddress.ShippingAddressList();
		addrListMagento.setShippingAddress(addrMagento);
		return Converter.convertMagentoShipAddrToMindShipAddr(addrListMagento);
	}

	public static ShippingMethodList getShippingMehod(Map<String, String> requestMap) {
		logger.info("Getting Shipping Methods for user - "+requestMap.get("username"));
		String input = "{ \"address\" : { \"region\" : "+requestMap.get("town")+", \"regionId\" : "+requestMap.get("addressId")+", \"countryId\" : "+requestMap.get("country")+", \"postCode\" : "+requestMap.get("postalCode")+"}}";
		HttpHeaders header = new HttpHeaders();
		header.add("Authorization", "Bearer "+requestMap.get("accessToken"));
		HttpEntity<String> entity = new HttpEntity<String>(input, header);
		String url = Constants.MAGENTO_SHIPPING_METHOD_URL;
		ShippingMethod shipMethMagento = new ShippingMethod();
		try {
	    	ObjectMapper mapper = new ObjectMapper();
		    JsonNode actObj = mapper.readTree(input);
			ResponseEntity<com.mindtree.pojo.magento.shippingAddress.ShippingMethodList> response = getRestTemplate().postForEntity(url, entity, com.mindtree.pojo.magento.shippingAddress.ShippingMethodList.class);
		} catch (RestClientException e) {
			logger.error(e.getMessage());
		} catch (KeyManagementException e) {
			logger.error(e.getMessage());
		} catch (KeyStoreException e) {
			logger.error(e.getMessage());
		} catch (NoSuchAlgorithmException e) {
			logger.error(e.getMessage());
		} catch (JsonProcessingException e) {
			logger.error(e.getMessage());
		} catch (IOException e) {
			logger.error(e.getMessage());
		}
		//to be implemented
		return null;
	}

	public static String placeOrder(Map<String, String> requestMap) {
		logger.info("Placing order for user - "+requestMap.get("username"));
		String intOrderId = "empty";
		String orderId = "empty";
		//this is hardcoded shipping info and payment option 
		String inputShipInfo = "{\"addressInformation\": {\"shippingAddress\": {\"region\": \"Alabama\",\"region_id\": 1,\"country_id\": \"US\",\"street\": [\"street 500\"],\"company\": \"mindtree\",\"telephone\": \"1234567890\",\"postcode\": \"90002\",\"city\": \"Alabama\",\"firstname\": \"pooja\",\"lastname\": \"puranik\",\"prefix\": \"address_\",\"sameAsBilling\": 1},\"billingAddress\": {\"region\": \"Alabama\",\"region_id\": 1,\"country_id\": \"US\",\"street\": [\"street 500\"],\"company\": \"mindtree\",\"telephone\": \"1231231231\",\"postcode\": \"90002\",\"city\": \"Alabama\",\"firstname\": \"pooja\",\"lastname\": \"puranik\",\"prefix\": \"address_\"},\"shipping_method_code\": \"freeshipping\",\"shipping_carrier_code\": \"freeshipping\"}}";
		String inputPayMeth = "{\"paymentMethod\":{\"method\":\"purchaseorder\"},\"shippingMethod\":{\"method_code\":\"freeshipping\",\"carrier_code\":\"freeshipping\",\"additionalProperties\":{}}}";
		HttpHeaders header = new HttpHeaders();
		header.add("Authorization", "Bearer "+requestMap.get("accessToken"));
		header.setContentType(MediaType.APPLICATION_JSON);
		HttpHeaders adminHeader = new HttpHeaders();
		adminHeader.add("Authorization", "Bearer 63f1fif2qurlx7isg6f3pknxf4tlqrjb");
		adminHeader.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<String> entityShipInfo = new HttpEntity<String>(inputShipInfo, header);
		HttpEntity<String> entityPayMeth = new HttpEntity<String>(inputPayMeth, header);
		HttpEntity<String> entityOrderDetails = new HttpEntity<String>(adminHeader);
		String urlShipInfo = Constants.MAGENTO_SHIP_INFO_URL;
		String urlPayMeth = Constants.MAGENTO_PAYMENT_METH_URL;
		String urlOrderDetails = Constants.MAGENTO_ORDER_DETAILS_URL;
		try {
			ResponseEntity<String> responseShipInfo = getRestTemplate().exchange(urlShipInfo, HttpMethod.POST, entityShipInfo, String.class);
			ResponseEntity<String> responsePayMeth = getRestTemplate().exchange(urlPayMeth, HttpMethod.PUT, entityPayMeth, String.class);
			intOrderId = responsePayMeth.getBody().replaceAll("^\"|\"$", "");
			urlOrderDetails = urlOrderDetails + intOrderId;
			ResponseEntity<Map> responseOrderDetails = getRestTemplate().exchange(urlOrderDetails, HttpMethod.GET, entityOrderDetails, Map.class);
			orderId = responseOrderDetails.getBody().get("increment_id").toString();
		} catch (RestClientException e) {
			logger.error(e.getMessage());
		} catch (KeyManagementException e) {
			logger.error(e.getMessage());
		} catch (KeyStoreException e) {
			logger.error(e.getMessage());
		} catch (NoSuchAlgorithmException e) {
			logger.error(e.getMessage());
		} 
		logger.info("Placed order for user - "+orderId);
		return orderId;
	}

}
