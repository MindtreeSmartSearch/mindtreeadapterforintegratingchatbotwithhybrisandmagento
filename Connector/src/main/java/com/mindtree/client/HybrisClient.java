package com.mindtree.client;

import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.HashMap;
import java.util.Map;
import javax.net.ssl.SSLContext;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustStrategy;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mindtree.constants.Constants;
import com.mindtree.controller.MindtreeController;
import com.mindtree.converter.Converter;
import com.mindtree.pojo.hybris.productDetails.ProductDetail;
import com.mindtree.pojo.hybris.products.ProductHybris;
import com.mindtree.pojo.mindtree.product.ProductMindtree;
import com.mindtree.pojo.mindtree.shippingAddress.ShippingAddressList;
import com.mindtree.pojo.mindtree.shippingAddress.ShippingMethodList;
import com.mindtree.pojo.mindtree.shippingAddress.ShippingMethods;
import com.mindtree.pojo.hybris.cart.Cart;

public class HybrisClient {

	private static final Logger logger = LoggerFactory.getLogger(MindtreeController.class);
	
	public static void main(String[] args) {
		//getProducts(Constants.HYBRIS_PRODUCT_URL);
		//getAnonymousCart();
		//getProductDetail("300464996");
		Map map = new HashMap<String, String>();
		/*map.put("username", "bipinkumar@mindtree.com");
		map.put("password", "pass1234");
		login(map);*/
		map.put("username", "bipinkumar@mindtree.com");
		map.put("guestCartId", "6868b4a2-2c10-4128-824e-4fbe2dda8ae4");
		map.put("accessToken", "c62b5fe5-3e89-4655-89d5-4b52c7c4d619");
		mergeCart(map);
		
	}
	
	public static ProductHybris getProducts(String url){
		
		ProductHybris prod = new ProductHybris();
		try {
			prod = getRestTemplate().getForObject(url, ProductHybris.class);
		} catch (RestClientException e) {
			logger.error(e.getMessage());
		} catch (KeyManagementException e) {
			logger.error(e.getMessage());
		} catch (KeyStoreException e) {
			logger.error(e.getMessage());
		} catch (NoSuchAlgorithmException e) {
			logger.error(e.getMessage());
		}
		logger.info("Printing response from hybris product search....."+prod.toString());
		return prod;
	}
	
	public static Cart getAnonymousCart(){
		Cart cart = null;
		try {
		 cart = getRestTemplate().postForObject(Constants.HYBRIS_ANONYMOUS_CART_URL, null, Cart.class);
		} catch (RestClientException e) {
			logger.error(e.getMessage());
		} catch (KeyManagementException e) {
			logger.error(e.getMessage());
		} catch (KeyStoreException e) {
			logger.error(e.getMessage());
		} catch (NoSuchAlgorithmException e) {
			logger.error(e.getMessage());
		}
		logger.info("printing anonymous cart from Hybris.."+cart.toString());
		return cart;
	}
	
	public static String addToAnonymousCart(String url){
		String addToCartMsg = "failure";
		Map<String, String> responseMap = new HashMap<String, String>();
		try {
			responseMap =  getRestTemplate().postForObject(url,null,Map.class);
		} catch (RestClientException e) {
			logger.error(e.getMessage());
		} catch (KeyManagementException e) {
			logger.error(e.getMessage());
		} catch (KeyStoreException e) {
			logger.error(e.getMessage());
		} catch (NoSuchAlgorithmException e) {
			logger.error(e.getMessage());
		}
		if(!responseMap.equals(null)){
			addToCartMsg = responseMap.get("statusCode").toString();
		}
		return addToCartMsg;
	}
	
	//Code to disable certificate check and hence avoiding SSL handshake issues.
	public static RestTemplate getRestTemplate() throws KeyStoreException, NoSuchAlgorithmException, KeyManagementException {
		ObjectMapper mapper =new ObjectMapper();    
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		TrustStrategy acceptingTrustStrategy = new TrustStrategy() {
	        @Override
	        public boolean isTrusted(X509Certificate[] x509Certificates, String s) throws CertificateException {
	            return true;
	        }
	    };
	    SSLContext sslContext = org.apache.http.ssl.SSLContexts.custom().loadTrustMaterial(null, acceptingTrustStrategy).build();
	    SSLConnectionSocketFactory csf = new SSLConnectionSocketFactory(sslContext, new NoopHostnameVerifier());
	    CloseableHttpClient httpClient = HttpClients.custom().setSSLSocketFactory(csf).build();
	    HttpComponentsClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory();
	    requestFactory.setHttpClient(httpClient);
	    RestTemplate restTemplate = new RestTemplate(requestFactory);
	    restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
	    return restTemplate;
	}
	
	public static String formSearchQuery(Map<String, String> requestMap){
	String searchQuery = "query="+requestMap.get("searchTerm")+":relevance";
	for(String key : requestMap.keySet()){
		if(!key.equalsIgnoreCase("commerceSystem")&&!key.equalsIgnoreCase("searchTerm"))
		searchQuery  = searchQuery +":"+key.toString()+":"+requestMap.get(key).toString();
	}
	return searchQuery;
	}

	public static ProductDetail getProductDetail(String itemId) {
	ProductDetail pd = new ProductDetail();
	try {
		String url = Constants.PRODUCT_DETAIL_HYBRIS.concat(itemId);
		logger.info("printing product detail url in hybris...."+url);
		pd = getRestTemplate().getForObject(url,new ProductDetail().getClass());
	} catch (RestClientException e) {
		logger.error(e.getMessage());
	} catch (KeyManagementException e) {
		logger.error(e.getMessage());
	} catch (KeyStoreException e) {
		logger.error(e.getMessage());
	} catch (NoSuchAlgorithmException e) {
		logger.error(e.getMessage());
	}
	return pd;	
	}
	
	public static String login(Map requestMap){
		logger.info("Login with hybris user "+requestMap.get("username"));
		String accessToken = "";
		Map<String, String>responseMap = new HashMap<String, String>();
		String url = Constants.HYBRIS_LOGIN_URL+"&username="+requestMap.get("username")+"&password="+requestMap.get("password");
		try {
			responseMap = getRestTemplate().postForObject(url, null, Map.class);
		} catch (RestClientException e) {
			logger.error(e.getMessage());
		} catch (KeyManagementException e) {
			logger.error(e.getMessage());
		} catch (KeyStoreException e) {
			logger.error(e.getMessage());
		} catch (NoSuchAlgorithmException e) {
			logger.error(e.getMessage());
		}
		if(!responseMap.isEmpty()&& responseMap != null){
			accessToken = responseMap.get("access_token");
		}
		logger.info("printing access token - "+accessToken);
		return accessToken;
	}
	
	public static String mergeCart(Map requestMap){
		logger.info("Merging guest user cart - "+requestMap.get("guestCartId")+" into "+requestMap.get("username")+"'s cart");
		String loggedInUserCartId = "";
		String url = Constants.HYBRIS_MERGE_CART.concat(requestMap.get("username").toString()).concat("/carts?oldCartId=").concat(requestMap.get("guestCartId").toString());
		Map responseMap = new HashMap<String, String>();
		HttpHeaders header = new HttpHeaders();
		header.add("Authorization", "Bearer "+requestMap.get("accessToken"));
		HttpEntity<String> entity = new HttpEntity<String>("parameters", header);
		try {
			ResponseEntity<Map> response = getRestTemplate().exchange(url, HttpMethod.POST, entity, Map.class);
			responseMap = response.getBody();
		} catch (RestClientException e) {
			logger.error(e.getMessage());
		} catch (KeyManagementException e) {
			logger.error(e.getMessage());
		} catch (KeyStoreException e) {
			logger.error(e.getMessage());
		} catch (NoSuchAlgorithmException e) {
			logger.error(e.getMessage());
		}
		logger.info("printing new cart id - "+responseMap.get("guid"));
		loggedInUserCartId = responseMap.get("code").toString();
		return loggedInUserCartId;
	}

	public static ShippingAddressList getShippingAddress(Map<String, String> requestMap) {
		logger.info("Getting shipping addresses for user - "+requestMap.get("username"));
		HttpHeaders header = new HttpHeaders();
		header.add("Authorization", "Bearer "+requestMap.get("accessToken"));
		HttpEntity<String> entity = new HttpEntity<String>("parameters", header);
		String url = Constants.HYBRIS_MERGE_CART.concat(requestMap.get("username")).concat("/addresses");
		ShippingAddressList addrListMind = new ShippingAddressList();
		try {
			ResponseEntity<com.mindtree.pojo.hybris.shippingAddress.ShippingAddressList> addrListHyb = getRestTemplate().exchange(url, HttpMethod.GET, entity, com.mindtree.pojo.hybris.shippingAddress.ShippingAddressList.class);
			addrListMind = Converter.convertHybrisShipAddrToMindShipAddr(addrListHyb.getBody());
		} catch (RestClientException e) {
			logger.error(e.getMessage());
		} catch (KeyManagementException e) {
			logger.error(e.getMessage());
		} catch (KeyStoreException e) {
			logger.error(e.getMessage());
		} catch (NoSuchAlgorithmException e) {
			logger.error(e.getMessage());
		}
		
		return addrListMind;
	}

	public static ShippingMethodList getShippingMethod(Map<String, String> requestMap) {
		logger.info("Getting shipping methods for user - "+requestMap.get("username"));
		//String url = Constants.HYBRIS_MERGE_CART.concat(requestMap.get("username")).concat("/carts/").concat(requestMap.get("cartId")).concat("/addresses/delivery?addressId=").concat(requestMap.get("addressId"));
		HttpHeaders header = new HttpHeaders();
		header.add("Authorization", "Bearer "+requestMap.get("accessToken"));
		HttpEntity<String> entity = new HttpEntity<String>("parameters", header);
		String url = Constants.HYBRIS_MERGE_CART.concat(requestMap.get("username")).concat("/carts/").concat(requestMap.get("cartId")).concat("/deliverymodes");
		com.mindtree.pojo.hybris.shippingAddress.ShippingMethods shipMethHybris = new com.mindtree.pojo.hybris.shippingAddress.ShippingMethods();
		try {
			ResponseEntity<com.mindtree.pojo.hybris.shippingAddress.ShippingMethods> response = getRestTemplate().exchange(url, HttpMethod.GET, entity, com.mindtree.pojo.hybris.shippingAddress.ShippingMethods.class);
			shipMethHybris = response.getBody();
		} catch (RestClientException e) {
			logger.error(e.getMessage());
		} catch (KeyManagementException e) {
			logger.error(e.getMessage());
		} catch (KeyStoreException e) {
			logger.error(e.getMessage());
		} catch (NoSuchAlgorithmException e) {
			logger.error(e.getMessage());
		}
		return Converter.convertHybrisShipMethodToMind(shipMethHybris);
	}

}
