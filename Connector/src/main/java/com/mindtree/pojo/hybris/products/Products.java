package com.mindtree.pojo.hybris.products;

import com.mindtree.pojo.hybris.products.Images;
import com.mindtree.pojo.hybris.products.Price;

public class Products {
	private String summary;

    private Price price;

    private String description;

    private String name;

    private Images[] images;

    private String code;

    public String getSummary ()
    {
        return summary;
    }

    public void setSummary (String summary)
    {
        this.summary = summary;
    }

    public Price getPrice ()
    {
        return price;
    }

    public void setPrice (Price price)
    {
        this.price = price;
    }

    public String getDescription ()
    {
        return description;
    }

    public void setDescription (String description)
    {
        this.description = description;
    }

    public String getName ()
    {
        return name;
    }

    public void setName (String name)
    {
        this.name = name;
    }

    public Images[] getImages ()
    {
        return images;
    }

    public void setImages (Images[] images)
    {
        this.images = images;
    }

    public String getCode ()
    {
        return code;
    }

    public void setCode (String code)
    {
        this.code = code;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [summary = "+summary+", price = "+price+", description = "+description+", name = "+name+", images = "+images+", code = "+code+"]";
    }
}
