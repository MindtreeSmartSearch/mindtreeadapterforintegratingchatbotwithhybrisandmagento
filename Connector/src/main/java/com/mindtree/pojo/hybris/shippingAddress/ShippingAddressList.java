package com.mindtree.pojo.hybris.shippingAddress;

public class ShippingAddressList {
	 private Addresses[] addresses;

	    public Addresses[] getAddresses ()
	    {
	        return addresses;
	    }

	    public void setAddresses (Addresses[] addresses)
	    {
	        this.addresses = addresses;
	    }

	    @Override
	    public String toString()
	    {
	        return "ClassPojo [addresses = "+addresses+"]";
	    }
}
