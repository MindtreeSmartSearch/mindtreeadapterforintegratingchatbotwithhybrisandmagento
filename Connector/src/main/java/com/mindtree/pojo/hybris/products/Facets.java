package com.mindtree.pojo.hybris.products;

import com.mindtree.pojo.hybris.products.TopValues;
import com.mindtree.pojo.hybris.products.Values;

public class Facets {
	private String multiSelect;

    private String category;

    private Values[] values;

    private String visible;

    private String priority;

    private String name;

    private TopValues[] topValues;

    public String getMultiSelect ()
    {
        return multiSelect;
    }

    public void setMultiSelect (String multiSelect)
    {
        this.multiSelect = multiSelect;
    }

    public String getCategory ()
    {
        return category;
    }

    public void setCategory (String category)
    {
        this.category = category;
    }

    public Values[] getValues ()
    {
        return values;
    }

    public void setValues (Values[] values)
    {
        this.values = values;
    }

    public String getVisible ()
    {
        return visible;
    }

    public void setVisible (String visible)
    {
        this.visible = visible;
    }

    public String getPriority ()
    {
        return priority;
    }

    public void setPriority (String priority)
    {
        this.priority = priority;
    }

    public String getName ()
    {
        return name;
    }

    public void setName (String name)
    {
        this.name = name;
    }

    public TopValues[] getTopValues ()
    {
        return topValues;
    }

    public void setTopValues (TopValues[] topValues)
    {
        this.topValues = topValues;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [multiSelect = "+multiSelect+", category = "+category+", values = "+values+", visible = "+visible+", priority = "+priority+", name = "+name+", topValues = "+topValues+"]";
    }
}
