package com.mindtree.pojo.hybris.products;

import com.mindtree.pojo.hybris.products.Query;

public class Values {
	private String selected;

    private String count;

    private Query query;

    private String name;

    public String getSelected ()
    {
        return selected;
    }

    public void setSelected (String selected)
    {
        this.selected = selected;
    }

    public String getCount ()
    {
        return count;
    }

    public void setCount (String count)
    {
        this.count = count;
    }

    public Query getQuery ()
    {
        return query;
    }

    public void setQuery (Query query)
    {
        this.query = query;
    }

    public String getName ()
    {
        return name;
    }

    public void setName (String name)
    {
        this.name = name;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [selected = "+selected+", count = "+count+", query = "+query+", name = "+name+"]";
    }
}
