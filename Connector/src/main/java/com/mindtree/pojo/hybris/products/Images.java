package com.mindtree.pojo.hybris.products;

public class Images {
	private String format;

    private String url;

    public String getFormat ()
    {
        return format;
    }

    public void setFormat (String format)
    {
        this.format = format;
    }

    public String getUrl ()
    {
        return url;
    }

    public void setUrl (String url)
    {
        this.url = url;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [format = "+format+", url = "+url+"]";
    }
}
