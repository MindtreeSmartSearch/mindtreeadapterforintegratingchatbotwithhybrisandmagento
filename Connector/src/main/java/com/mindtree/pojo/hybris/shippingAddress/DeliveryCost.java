package com.mindtree.pojo.hybris.shippingAddress;

public class DeliveryCost {
	private String currencyIso;

    private String value;

    private String priceType;

    private String formattedValue;

    public String getCurrencyIso ()
    {
        return currencyIso;
    }

    public void setCurrencyIso (String currencyIso)
    {
        this.currencyIso = currencyIso;
    }

    public String getValue ()
    {
        return value;
    }

    public void setValue (String value)
    {
        this.value = value;
    }

    public String getPriceType ()
    {
        return priceType;
    }

    public void setPriceType (String priceType)
    {
        this.priceType = priceType;
    }

    public String getFormattedValue ()
    {
        return formattedValue;
    }

    public void setFormattedValue (String formattedValue)
    {
        this.formattedValue = formattedValue;
    }
}
