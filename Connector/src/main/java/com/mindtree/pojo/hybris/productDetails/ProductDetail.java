package com.mindtree.pojo.hybris.productDetails;

public class ProductDetail {
	private String summary;

    private Stock stock;

    private PriceRange priceRange;

    private Price price;

    private String purchasable;

    private String numberOfReviews;

    private String description;

    private String name;

    private BaseOptions[] baseOptions;

    private String baseProduct;

    private Categories[] categories;

    private String availableForPickup;

    private String code;

    private String url;

    public String getSummary ()
    {
        return summary;
    }

    public void setSummary (String summary)
    {
        this.summary = summary;
    }

    public Stock getStock ()
    {
        return stock;
    }

    public void setStock (Stock stock)
    {
        this.stock = stock;
    }

    public PriceRange getPriceRange ()
    {
        return priceRange;
    }

    public void setPriceRange (PriceRange priceRange)
    {
        this.priceRange = priceRange;
    }

    public Price getPrice ()
    {
        return price;
    }

    public void setPrice (Price price)
    {
        this.price = price;
    }

    public String getPurchasable ()
    {
        return purchasable;
    }

    public void setPurchasable (String purchasable)
    {
        this.purchasable = purchasable;
    }

    public String getNumberOfReviews ()
    {
        return numberOfReviews;
    }

    public void setNumberOfReviews (String numberOfReviews)
    {
        this.numberOfReviews = numberOfReviews;
    }

    public String getDescription ()
    {
        return description;
    }

    public void setDescription (String description)
    {
        this.description = description;
    }

    public String getName ()
    {
        return name;
    }

    public void setName (String name)
    {
        this.name = name;
    }

    public BaseOptions[] getBaseOptions ()
    {
        return baseOptions;
    }

    public void setBaseOptions (BaseOptions[] baseOptions)
    {
        this.baseOptions = baseOptions;
    }

    public String getBaseProduct ()
    {
        return baseProduct;
    }

    public void setBaseProduct (String baseProduct)
    {
        this.baseProduct = baseProduct;
    }

    public Categories[] getCategories ()
    {
        return categories;
    }

    public void setCategories (Categories[] categories)
    {
        this.categories = categories;
    }

    public String getAvailableForPickup ()
    {
        return availableForPickup;
    }

    public void setAvailableForPickup (String availableForPickup)
    {
        this.availableForPickup = availableForPickup;
    }

    public String getCode ()
    {
        return code;
    }

    public void setCode (String code)
    {
        this.code = code;
    }

    public String getUrl ()
    {
        return url;
    }

    public void setUrl (String url)
    {
        this.url = url;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [summary = "+summary+", stock = "+stock+", priceRange = "+priceRange+", price = "+price+", purchasable = "+purchasable+", numberOfReviews = "+numberOfReviews+", description = "+description+", name = "+name+", baseOptions = "+baseOptions+", baseProduct = "+baseProduct+", categories = "+categories+", availableForPickup = "+availableForPickup+", code = "+code+", url = "+url+"]";
    }
}
