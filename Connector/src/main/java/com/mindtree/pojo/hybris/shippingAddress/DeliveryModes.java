package com.mindtree.pojo.hybris.shippingAddress;

public class DeliveryModes {
	private String description;

    private String name;

    private String code;

    private DeliveryCost deliveryCost;

    public String getDescription ()
    {
        return description;
    }

    public void setDescription (String description)
    {
        this.description = description;
    }

    public String getName ()
    {
        return name;
    }

    public void setName (String name)
    {
        this.name = name;
    }

    public String getCode ()
    {
        return code;
    }

    public void setCode (String code)
    {
        this.code = code;
    }

    public DeliveryCost getDeliveryCost ()
    {
        return deliveryCost;
    }

    public void setDeliveryCost (DeliveryCost deliveryCost)
    {
        this.deliveryCost = deliveryCost;
    }
}
