package com.mindtree.pojo.hybris.products;

public class Query {
	private String url;
	private InnerQuery query;

	    public String getUrl ()
	    {
	        return url;
	    }

	    public void setUrl (String url)
	    {
	        this.url = url;
	    }

	    @Override
	    public String toString()
	    {
	        return "ClassPojo [url = "+url+", query = "+query+"]";
	    }

		public InnerQuery getQuery() {
			return query;
		}

		public void setQuery(InnerQuery query) {
			this.query = query;
		}
}
