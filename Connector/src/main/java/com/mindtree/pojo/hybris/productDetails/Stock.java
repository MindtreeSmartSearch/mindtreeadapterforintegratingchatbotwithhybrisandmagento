package com.mindtree.pojo.hybris.productDetails;

public class Stock {
	private String stockLevelStatus;

    private String stockLevel;

    public String getStockLevelStatus ()
    {
        return stockLevelStatus;
    }

    public void setStockLevelStatus (String stockLevelStatus)
    {
        this.stockLevelStatus = stockLevelStatus;
    }

    public String getStockLevel ()
    {
        return stockLevel;
    }

    public void setStockLevel (String stockLevel)
    {
        this.stockLevel = stockLevel;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [stockLevelStatus = "+stockLevelStatus+", stockLevel = "+stockLevel+"]";
    }
}
