package com.mindtree.pojo.hybris.productDetails;

public class BaseOptions {
	 private Selected selected;

	    private String variantType;

	    private Options[] options;

	    public Selected getSelected ()
	    {
	        return selected;
	    }

	    public void setSelected (Selected selected)
	    {
	        this.selected = selected;
	    }

	    public String getVariantType ()
	    {
	        return variantType;
	    }

	    public void setVariantType (String variantType)
	    {
	        this.variantType = variantType;
	    }

	    public Options[] getOptions ()
	    {
	        return options;
	    }

	    public void setOptions (Options[] options)
	    {
	        this.options = options;
	    }

	    @Override
	    public String toString()
	    {
	        return "ClassPojo [selected = "+selected+", variantType = "+variantType+", options = "+options+"]";
	    }
}
