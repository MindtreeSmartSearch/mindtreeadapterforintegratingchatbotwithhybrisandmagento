package com.mindtree.pojo.hybris.shippingAddress;

public class ShippingMethods {
	private DeliveryModes[] deliveryModes;

    public DeliveryModes[] getDeliveryModes ()
    {
        return deliveryModes;
    }

    public void setDeliveryModes (DeliveryModes[] deliveryModes)
    {
        this.deliveryModes = deliveryModes;
    }
}
