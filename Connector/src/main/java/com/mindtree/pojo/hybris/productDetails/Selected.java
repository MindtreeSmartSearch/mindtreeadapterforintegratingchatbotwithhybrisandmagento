package com.mindtree.pojo.hybris.productDetails;

public class Selected {
	private variantOptionQualifiers[] variantOptionQualifiers;

    private StockInBaseOptions stock;

    private PriceData priceData;

    private String code;

    private String url;

    public variantOptionQualifiers[] getVariantOptionQualifiers ()
    {
        return variantOptionQualifiers;
    }

    public void setVariantOptionQualifiers (variantOptionQualifiers[] variantOptionQualifiers)
    {
        this.variantOptionQualifiers = variantOptionQualifiers;
    }

    public StockInBaseOptions getStock ()
    {
        return stock;
    }

    public void setStock (StockInBaseOptions stock)
    {
        this.stock = stock;
    }

    public PriceData getPriceData ()
    {
        return priceData;
    }

    public void setPriceData (PriceData priceData)
    {
        this.priceData = priceData;
    }

    public String getCode ()
    {
        return code;
    }

    public void setCode (String code)
    {
        this.code = code;
    }

    public String getUrl ()
    {
        return url;
    }

    public void setUrl (String url)
    {
        this.url = url;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [variantOptionQualifiers = "+variantOptionQualifiers+", stock = "+stock+", priceData = "+priceData+", code = "+code+", url = "+url+"]";
    }
}
