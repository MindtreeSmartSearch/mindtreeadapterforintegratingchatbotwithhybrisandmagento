package com.mindtree.pojo.hybris.productDetails;

public class PriceRange {
	private MaxPrice maxPrice;

    private MinPrice minPrice;

    public MaxPrice getMaxPrice ()
    {
        return maxPrice;
    }

    public void setMaxPrice (MaxPrice maxPrice)
    {
        this.maxPrice = maxPrice;
    }

    public MinPrice getMinPrice ()
    {
        return minPrice;
    }

    public void setMinPrice (MinPrice minPrice)
    {
        this.minPrice = minPrice;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [maxPrice = "+maxPrice+", minPrice = "+minPrice+"]";
    }
}
