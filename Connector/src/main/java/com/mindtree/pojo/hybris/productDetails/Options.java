package com.mindtree.pojo.hybris.productDetails;

public class Options {
	private variantOptionQualifiers[] variantOptionQualifiers;

    private Stock stock;

    private PriceData priceData;

    private String code;

    private String url;

    public variantOptionQualifiers[] getVariantOptionQualifiers ()
    {
        return variantOptionQualifiers;
    }

    public void setVariantOptionQualifiers (variantOptionQualifiers[] variantOptionQualifiers)
    {
        this.variantOptionQualifiers = variantOptionQualifiers;
    }

    public Stock getStock ()
    {
        return stock;
    }

    public void setStock (Stock stock)
    {
        this.stock = stock;
    }

    public PriceData getPriceData ()
    {
        return priceData;
    }

    public void setPriceData (PriceData priceData)
    {
        this.priceData = priceData;
    }

    public String getCode ()
    {
        return code;
    }

    public void setCode (String code)
    {
        this.code = code;
    }

    public String getUrl ()
    {
        return url;
    }

    public void setUrl (String url)
    {
        this.url = url;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [variantOptionQualifiers = "+variantOptionQualifiers+", stock = "+stock+", priceData = "+priceData+", code = "+code+", url = "+url+"]";
    }
}
