package com.mindtree.pojo.hybris.productDetails;

public class variantOptionQualifiers {
	private String name;

    private String value;

    private Image image;

    private String qualifier;

    public String getName ()
    {
        return name;
    }

    public void setName (String name)
    {
        this.name = name;
    }

    public String getValue ()
    {
        return value;
    }

    public void setValue (String value)
    {
        this.value = value;
    }

    public Image getImage ()
    {
        return image;
    }

    public void setImage (Image image)
    {
        this.image = image;
    }

    public String getQualifier ()
    {
        return qualifier;
    }

    public void setQualifier (String qualifier)
    {
        this.qualifier = qualifier;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [name = "+name+", value = "+value+", image = "+image+", qualifier = "+qualifier+"]";
    }
}
