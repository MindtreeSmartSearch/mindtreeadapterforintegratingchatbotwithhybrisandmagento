package com.mindtree.pojo.hybris.shippingAddress;

public class Addresses {
	private String id;

    private String lastName;

    private String postalCode;

    private String town;

    private String line1;

    private String firstName;

    private String line2;

    private Country country;

    public String getId ()
    {
        return id;
    }

    public void setId (String id)
    {
        this.id = id;
    }

    public String getLastName ()
    {
        return lastName;
    }

    public void setLastName (String lastName)
    {
        this.lastName = lastName;
    }

    public String getPostalCode ()
    {
        return postalCode;
    }

    public void setPostalCode (String postalCode)
    {
        this.postalCode = postalCode;
    }

    public String getTown ()
    {
        return town;
    }

    public void setTown (String town)
    {
        this.town = town;
    }

    public String getLine1 ()
    {
        return line1;
    }

    public void setLine1 (String line1)
    {
        this.line1 = line1;
    }

    public String getFirstName ()
    {
        return firstName;
    }

    public void setFirstName (String firstName)
    {
        this.firstName = firstName;
    }

    public String getLine2 ()
    {
        return line2;
    }

    public void setLine2 (String line2)
    {
        this.line2 = line2;
    }

    public Country getCountry ()
    {
        return country;
    }

    public void setCountry (Country country)
    {
        this.country = country;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [id = "+id+", lastName = "+lastName+", postalCode = "+postalCode+", town = "+town+", line1 = "+line1+", firstName = "+firstName+", line2 = "+line2+", country = "+country+"]";
    }
}
