package com.mindtree.pojo.hybris.productDetails;

public class StockInBaseOptions {
	private String stockLevel;

    public String getStockLevel ()
    {
        return stockLevel;
    }

    public void setStockLevel (String stockLevel)
    {
        this.stockLevel = stockLevel;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [stockLevel = "+stockLevel+"]";
    }
}
