package com.mindtree.pojo.hybris.products;

public class Pagination {
	private String sort;

    private String totalResults;

    private String pageSize;

    private String currentPage;

    private String totalPages;

    public String getSort ()
    {
        return sort;
    }

    public void setSort (String sort)
    {
        this.sort = sort;
    }

    public String getTotalResults ()
    {
        return totalResults;
    }

    public void setTotalResults (String totalResults)
    {
        this.totalResults = totalResults;
    }

    public String getPageSize ()
    {
        return pageSize;
    }

    public void setPageSize (String pageSize)
    {
        this.pageSize = pageSize;
    }

    public String getCurrentPage ()
    {
        return currentPage;
    }

    public void setCurrentPage (String currentPage)
    {
        this.currentPage = currentPage;
    }

    public String getTotalPages ()
    {
        return totalPages;
    }

    public void setTotalPages (String totalPages)
    {
        this.totalPages = totalPages;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [sort = "+sort+", totalResults = "+totalResults+", pageSize = "+pageSize+", currentPage = "+currentPage+", totalPages = "+totalPages+"]";
    }
}
