package com.mindtree.pojo.hybris.products;

import com.mindtree.pojo.hybris.products.Facets;
import com.mindtree.pojo.hybris.products.Pagination;
import com.mindtree.pojo.hybris.products.Products;

public class ProductHybris {
	 private Facets[] facets;

	    private Pagination pagination;

	    private String type;

	    private Products[] products;

	    public Facets[] getFacets ()
	    {
	        return facets;
	    }

	    public void setFacets (Facets[] facets)
	    {
	        this.facets = facets;
	    }

	    public Pagination getPagination ()
	    {
	        return pagination;
	    }

	    public void setPagination (Pagination pagination)
	    {
	        this.pagination = pagination;
	    }

	    public String getType ()
	    {
	        return type;
	    }

	    public void setType (String type)
	    {
	        this.type = type;
	    }

	    public Products[] getProducts ()
	    {
	        return products;
	    }

	    public void setProducts (Products[] products)
	    {
	        this.products = products;
	    }

	    @Override
	    public String toString()
	    {
	        return "ProductHybris [facets = "+facets+", pagination = "+pagination+", type = "+type+", products = "+products+"]";
	    }
}
