package com.mindtree.pojo.hybris.shippingAddress;

public class Country {
	private String isocode;

    public String getIsocode ()
    {
        return isocode;
    }

    public void setIsocode (String isocode)
    {
        this.isocode = isocode;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [isocode = "+isocode+"]";
    }
}
