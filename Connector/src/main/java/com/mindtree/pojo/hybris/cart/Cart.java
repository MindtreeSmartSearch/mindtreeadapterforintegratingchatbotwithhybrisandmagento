package com.mindtree.pojo.hybris.cart;

public class Cart {
	private String guid;

    private String totalItems;

    private String[] entries;

    private TotalPriceWithTax totalPriceWithTax;

    private String code;

    private String type;

    private TotalPrice totalPrice;

    public String getGuid ()
    {
        return guid;
    }

    public void setGuid (String guid)
    {
        this.guid = guid;
    }

    public String getTotalItems ()
    {
        return totalItems;
    }

    public void setTotalItems (String totalItems)
    {
        this.totalItems = totalItems;
    }

    public String[] getEntries ()
    {
        return entries;
    }

    public void setEntries (String[] entries)
    {
        this.entries = entries;
    }

    public TotalPriceWithTax getTotalPriceWithTax ()
    {
        return totalPriceWithTax;
    }

    public void setTotalPriceWithTax (TotalPriceWithTax totalPriceWithTax)
    {
        this.totalPriceWithTax = totalPriceWithTax;
    }

    public String getCode ()
    {
        return code;
    }

    public void setCode (String code)
    {
        this.code = code;
    }

    public String getType ()
    {
        return type;
    }

    public void setType (String type)
    {
        this.type = type;
    }

    public TotalPrice getTotalPrice ()
    {
        return totalPrice;
    }

    public void setTotalPrice (TotalPrice totalPrice)
    {
        this.totalPrice = totalPrice;
    }

    @Override
    public String toString()
    {
        return "Cart [guid = "+guid+", totalItems = "+totalItems+", entries = "+entries+", totalPriceWithTax = "+totalPriceWithTax+", code = "+code+", type = "+type+", totalPrice = "+totalPrice+"]";
    }
}
