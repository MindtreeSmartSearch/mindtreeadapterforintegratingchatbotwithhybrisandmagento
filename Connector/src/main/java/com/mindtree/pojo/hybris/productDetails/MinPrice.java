package com.mindtree.pojo.hybris.productDetails;

public class MinPrice {
	private String currencyIso;

    private String value;

    public String getCurrencyIso ()
    {
        return currencyIso;
    }

    public void setCurrencyIso (String currencyIso)
    {
        this.currencyIso = currencyIso;
    }

    public String getValue ()
    {
        return value;
    }

    public void setValue (String value)
    {
        this.value = value;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [currencyIso = "+currencyIso+", value = "+value+"]";
    }
}
