package com.mindtree.pojo.mindtree.product;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Facets {

	private String name;
	
	private Values[] values;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Values[] getValues() {
		return values;
	}

	public void setValues(Values[] values) {
		this.values = values;
	}
	
	@Override
	public String toString()
	{
	    return "Class Fcaets [name = "+name+", values = "+values+"]";
	}
	
}
