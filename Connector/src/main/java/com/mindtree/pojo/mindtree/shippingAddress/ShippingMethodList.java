package com.mindtree.pojo.mindtree.shippingAddress;

public class ShippingMethodList {
	private ShippingMethods[] shipMethods;

	public ShippingMethods[] getShipMethods() {
		return shipMethods;
	}

	public void setShipMethods(ShippingMethods[] shipMethods) {
		this.shipMethods = shipMethods;
	}
	
}
