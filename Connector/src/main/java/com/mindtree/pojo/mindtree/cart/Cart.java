package com.mindtree.pojo.mindtree.cart;

public class Cart {
	
	private String cartid;
	
	public String getCartid() {
		return cartid;
	}

	public void setCartid(String cartid) {
		this.cartid = cartid;
	}

	@Override
    public String toString()
    {
        return "Cart [cartid = "+cartid+"]";
    }
}
