package com.mindtree.pojo.mindtree.product;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ProductMindtree {
	
 private Facets[] facets;
 
 private Products[] products;

public Facets[] getFacets() {
	return facets;
}

public void setFacets(Facets[] facets) {
	this.facets = facets;
}

public Products[] getProducts() {
	return products;
}

public void setProducts(Products[] products) {
	this.products = products;
}
 
@Override
public String toString()
{
    return "Class ProductMindtree [facets = "+facets+", products = "+products+"]";
}
 
}
