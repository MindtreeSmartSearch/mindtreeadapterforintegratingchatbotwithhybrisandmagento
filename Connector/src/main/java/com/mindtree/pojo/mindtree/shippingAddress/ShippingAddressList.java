package com.mindtree.pojo.mindtree.shippingAddress;

public class ShippingAddressList {

	private ShippingAddress[] shipAddr;

	public ShippingAddress[] getShipAddr() {
		return shipAddr;
	}

	public void setShipAddr(ShippingAddress[] shipAddr) {
		this.shipAddr = shipAddr;
	}
}
