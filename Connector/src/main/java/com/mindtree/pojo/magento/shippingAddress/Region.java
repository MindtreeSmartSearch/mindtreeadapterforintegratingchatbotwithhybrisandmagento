package com.mindtree.pojo.magento.shippingAddress;

public class Region {

	 private String region;

	    private String region_code;

	    private String region_id;

	    public String getRegion ()
	    {
	        return region;
	    }

	    public void setRegion (String region)
	    {
	        this.region = region;
	    }

	    public String getRegion_code ()
	    {
	        return region_code;
	    }

	    public void setRegion_code (String region_code)
	    {
	        this.region_code = region_code;
	    }

	    public String getRegion_id ()
	    {
	        return region_id;
	    }

	    public void setRegion_id (String region_id)
	    {
	        this.region_id = region_id;
	    }
	    
}
