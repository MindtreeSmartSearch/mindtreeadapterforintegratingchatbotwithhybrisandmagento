package com.mindtree.pojo.magento.product;

public class Filters {
	 private String field;

	    private String condition_type;

	    private String value;

	    public String getField ()
	    {
	        return field;
	    }

	    public void setField (String field)
	    {
	        this.field = field;
	    }

	    public String getCondition_type ()
	    {
	        return condition_type;
	    }

	    public void setCondition_type (String condition_type)
	    {
	        this.condition_type = condition_type;
	    }

	    public String getValue ()
	    {
	        return value;
	    }

	    public void setValue (String value)
	    {
	        this.value = value;
	    }

	    @Override
	    public String toString()
	    {
	        return "ClassPojo [field = "+field+", condition_type = "+condition_type+", value = "+value+"]";
	    }
}
