package com.mindtree.pojo.magento.shippingAddress;

public class ShippingAddress {
	 private Region region;

	    private String id;

	    private String company;

	    private String[] street;

	    private String country_id;

	    private String region_id;

	    private String lastname;

	    private String firstname;

	    private String postcode;

	    private String telephone;

	    private String customer_id;

	    private String city;

	    public Region getRegion ()
	    {
	        return region;
	    }

	    public void setRegion (Region region)
	    {
	        this.region = region;
	    }

	    public String getId ()
	    {
	        return id;
	    }

	    public void setId (String id)
	    {
	        this.id = id;
	    }

	    public String getCompany ()
	    {
	        return company;
	    }

	    public void setCompany (String company)
	    {
	        this.company = company;
	    }

	    public String[] getStreet ()
	    {
	        return street;
	    }

	    public void setStreet (String[] street)
	    {
	        this.street = street;
	    }

	    public String getCountry_id ()
	    {
	        return country_id;
	    }

	    public void setCountry_id (String country_id)
	    {
	        this.country_id = country_id;
	    }

	    public String getRegion_id ()
	    {
	        return region_id;
	    }

	    public void setRegion_id (String region_id)
	    {
	        this.region_id = region_id;
	    }

	    public String getLastname ()
	    {
	        return lastname;
	    }

	    public void setLastname (String lastname)
	    {
	        this.lastname = lastname;
	    }

	    public String getFirstname ()
	    {
	        return firstname;
	    }

	    public void setFirstname (String firstname)
	    {
	        this.firstname = firstname;
	    }

	    public String getPostcode ()
	    {
	        return postcode;
	    }

	    public void setPostcode (String postcode)
	    {
	        this.postcode = postcode;
	    }

	    public String getTelephone ()
	    {
	        return telephone;
	    }

	    public void setTelephone (String telephone)
	    {
	        this.telephone = telephone;
	    }

	    public String getCustomer_id ()
	    {
	        return customer_id;
	    }

	    public void setCustomer_id (String customer_id)
	    {
	        this.customer_id = customer_id;
	    }

	    public String getCity ()
	    {
	        return city;
	    }

	    public void setCity (String city)
	    {
	        this.city = city;
	    }
}
