package com.mindtree.pojo.magento.product;

public class Filter_groups {

	private Filters[] filters;

    public Filters[] getFilters ()
    {
        return filters;
    }

    public void setFilters (Filters[] filters)
    {
        this.filters = filters;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [filters = "+filters+"]";
    }
}
