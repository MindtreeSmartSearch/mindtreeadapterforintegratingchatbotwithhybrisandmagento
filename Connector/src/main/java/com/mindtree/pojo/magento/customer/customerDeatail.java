package com.mindtree.pojo.magento.customer;

import com.mindtree.pojo.magento.shippingAddress.ShippingAddress;

public class customerDeatail {

	private String id;
	private String group_id;
	private String created_at;
	private String updated_at;
	private String created_in;
	private String email;
	private String firstname;
	private String lastname;
	private String store_id;
	private String website_id;
	private String disable_auto_group_change;
	private ShippingAddress[] addresses;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getGroup_id() {
		return group_id;
	}
	public void setGroup_id(String group_id) {
		this.group_id = group_id;
	}
	public String getCreated_at() {
		return created_at;
	}
	public void setCreated_at(String created_at) {
		this.created_at = created_at;
	}
	public String getUpdated_at() {
		return updated_at;
	}
	public void setUpdated_at(String updated_at) {
		this.updated_at = updated_at;
	}
	public String getCreated_in() {
		return created_in;
	}
	public void setCreated_in(String created_in) {
		this.created_in = created_in;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getFirstname() {
		return firstname;
	}
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	public String getStore_id() {
		return store_id;
	}
	public void setStore_id(String store_id) {
		this.store_id = store_id;
	}
	public String getWebsite_id() {
		return website_id;
	}
	public void setWebsite_id(String website_id) {
		this.website_id = website_id;
	}
	public String getDisable_auto_group_change() {
		return disable_auto_group_change;
	}
	public void setDisable_auto_group_change(String disable_auto_group_change) {
		this.disable_auto_group_change = disable_auto_group_change;
	}
	public ShippingAddress[] getAddresses() {
		return addresses;
	}
	public void setAdresses(ShippingAddress[] addresses) {
		this.addresses = addresses;
	}
	
}
