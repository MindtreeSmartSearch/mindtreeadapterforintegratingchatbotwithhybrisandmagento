package com.mindtree.pojo.magento.shippingAddress;

public class ShippingAddressList {
	
	private ShippingAddress[] shippingAddress;

	public ShippingAddress[] getShippingAddress() {
		return shippingAddress;
	}

	public void setShippingAddress(ShippingAddress[] shippingAddress) {
		this.shippingAddress = shippingAddress;
	}
	
}
