package com.mindtree.pojo.magento.cart;

public class addToCart {
	private CartItem cartItem;

    public CartItem getCartItem ()
    {
        return cartItem;
    }

    public void setCartItem (CartItem cartItem)
    {
        this.cartItem = cartItem;
    }

    @Override
    public String toString()
    {
        return "AddToCart json [cartItem = "+cartItem+"]";
    }
}
