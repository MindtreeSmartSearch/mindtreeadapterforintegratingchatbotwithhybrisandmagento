package com.mindtree.pojo.magento.shippingAddress;

public class ShippingMethod {
 
	private String  carrier_code;
	private String  method_code;
	private String  carrier_title;
	private String  method_title;
	private String  amount;
	private String  available;
	public String getCarrier_code() {
		return carrier_code;
	}
	public void setCarrier_code(String carrier_code) {
		this.carrier_code = carrier_code;
	}
	public String getMethod_code() {
		return method_code;
	}
	public void setMethod_code(String method_code) {
		this.method_code = method_code;
	}
	public String getCarrier_title() {
		return carrier_title;
	}
	public void setCarrier_title(String carrier_title) {
		this.carrier_title = carrier_title;
	}
	public String getMethod_title() {
		return method_title;
	}
	public void setMethod_title(String method_title) {
		this.method_title = method_title;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getAvailable() {
		return available;
	}
	public void setAvailable(String available) {
		this.available = available;
	}
}
