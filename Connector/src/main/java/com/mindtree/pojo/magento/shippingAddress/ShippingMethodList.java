package com.mindtree.pojo.magento.shippingAddress;

public class ShippingMethodList {

	private ShippingMethod[] shipMethod;

	public ShippingMethod[] getShipMethod() {
		return shipMethod;
	}

	public void setShipMethod(ShippingMethod[] shipMethod) {
		this.shipMethod = shipMethod;
	}
}
