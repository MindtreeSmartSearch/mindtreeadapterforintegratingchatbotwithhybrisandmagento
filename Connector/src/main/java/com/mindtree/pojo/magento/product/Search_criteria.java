package com.mindtree.pojo.magento.product;

public class Search_criteria {
	private Filter_groups[] filter_groups;

    public Filter_groups[] getFilter_groups ()
    {
        return filter_groups;
    }

    public void setFilter_groups (Filter_groups[] filter_groups)
    {
        this.filter_groups = filter_groups;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [filter_groups = "+filter_groups+"]";
    }
	
}
