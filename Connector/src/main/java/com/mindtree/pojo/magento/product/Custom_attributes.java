package com.mindtree.pojo.magento.product;

public class Custom_attributes {
	private String value;

    private String attribute_code;

    public String getValue ()
    {
        return value;
    }

    public void setValue (String value)
    {
        this.value = value;
    }

    public String getAttribute_code ()
    {
        return attribute_code;
    }

    public void setAttribute_code (String attribute_code)
    {
        this.attribute_code = attribute_code;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [value = "+value+", attribute_code = "+attribute_code+"]";
    }
}
